case year
when 1894
  "Born."
when 1895..1913
  "Childhood in Lousville, Winston Co., Mississippi."
else
  "No information about this year."
end
#Is identical to:
if 1894 === year
  "Born."
elsif (1895..1913) === year
  "Childhood in Lousville, Winston Co., Mississippi."
else
  "No information about this year."
end

#2. A Castle Has Its Computers
#
#require 'endertromb'
class WishMaker
  def initialize
    @energy = rand( 6 )
  end
  def grant( wish )
    if wish.length > 10 or wish.include? ' '
      raise ArgumentError, "Bad wish."
    end
    if @energy.zero?
      raise Exception, "No energy left."
    end
    @energy -= 1
    Endertromb::make( wish )
  end
end
number = 5
print number.next                   # prints '6'
print 5.class                       # prints 'Integer'
print 'wishing for antlers'.class   # prints 'String'
print WishMaker.new.class           # prints 'WishMaker'

#require 'endertromb'
class MindReader
  def initialize
    @minds = Endertromb::scan_for_sentience
  end
  def read
    @minds.collect do |mind|
       mind.read
    end
  end
end


#irb
Object::constants
MindReader::class_variables
MindReader::methos
class Elevator
  def initialize( pass )
    raise AccessDeniedError, "bad password" \
      unless pass.equals? @@maintenance_password
  end
end

#3. The Continued Story of My Daughter’s Organ Instructor
def wipe_mutterings_from( sentence )
  while sentence.include? '('
    open = sentence.index( '(' )
    close = sentence.index( ')', open )
    sentence[open..close] = '' if close
  end
end
what_he_said = "But, strangely (em-pithy-dah),
  I learned upon, played upon (pon-shoo) the
  organs on my home (oth-rea) planet."
wipe_mutterings_from( what_he_said )
print what_he_said

def wipe_mutterings_from( sentence )
  unless sentence.respond_to? :include?
    raise ArgumentError,
      "cannot wipe mutterings from a #{ sentence.class }"
  end
  while sentence.include? '('
    open = sentence.index( '(' )
    close = sentence.index( ')', open )
    sentence[open..close] = '' if close
  end
end
def wipe_mutterings_from( sentence )
  unless sentence.respond_to? :include?
    raise ArgumentError,
      "cannot wipe mutterings from a #{ sentence.class }"
  end
  sentence = sentence.dup
  while sentence.include? '('
    open = sentence.index( '(' )
    close = sentence.index( ')', open )
    sentence[open..close] = '' if close
  end
  sentence
end

def wipe_mutterings_from( sentence )
  unless sentence.respond_to? :include?
    raise ArgumentError,
      "cannot wipe mutterings from a #{ sentence.class }"
  end
  sentence = sentence.dup
  while sentence.include? '('
    open = sentence.index( '(' )
    close = sentence.index( ')', open )
    sentence[open..close] = '' if close
  end
  sentence
end

def wipe_mutterings_from( sentence )
  unless sentence.respond_to? :gsub
    raise ArgumentError,
      "cannot wipe mutterings from a #{ sentence.class }"
  end
  sentence.gsub( /\([-\w]+\)/, '' )
end


#The Mechanisms of Name-Calling
class String

  # The parts of my daughter's organ
  # instructor's name.
  @@syllables = [
    { 'Paij' => 'Personal',
      'Gonk' => 'Business',
      'Blon' => 'Slave',
      'Stro' => 'Master',
      'Wert' => 'Father',
      'Onnn' => 'Mother' },
    { 'ree'  => 'AM',
      'plo'  => 'PM' }
  ]

  # A method to determine what a
  # certain name of his means.
  def name_significance
    parts = self.split( '-' )
    syllables = @@syllables.dup
    signif = parts.collect do |p|
      syllables.shift[p]
    end
    signif.join( ' ' )
  end
end

class String
  def dash_split
    self.split( '-' )
  end
end
class String
  def dash_split; split( '-' ); end
end

#4. The Goat Wants to Watch a Whole Film
class ArrayMine < Array
  # Build a string from this array, formatting each entry
  # then joining them together.
  def join( sep = $,, format = "%s" )
    collect do |item|
      sprintf( format, item )
    end.join( sep )
  end
end

# See, here is the module -- where else could our code possibly stay?
module WatchfulSaintAgnes
  # A CONSTANT is laying here by the doorway.  Fine.
  TOOTHLESS_MAN_WITH_FORK = ['man', 'fork', 'exposed gums']
  # A Class is eating, living well in the kitchen.
  class FatWaxyChild; end
  # A Method is hiding back in the banana closet, God knows why.
  def timid_foxfaced_girl; {'please' => 'i want an acorn please'}; end
end
WatchfulSaintAgnes::TOOTHLESS_MAN_WITH_FORK
WatchfulSaintAgnes::FatWaxyChild.new
WatchfulSaintAgnes::instance_methods

class TheTimeWarnerAolCitibankCaringAndLovingFacility; end
TheTimeWarnerAolCitibankCaringAndLovingFacility.extend WatchfulSaintAgnes




#########################################
# 5. The Theft of the Lottery Captain   #
#########################################
class Ticket

  def initialize(*ns)
    if ns.size != 3
      raise ArgumentError, 'Tem que ser apenas 3 números!'
    end
    if ns.uniq.size != ns.size
      raise ArgumentError, 'Combinação só números não repetidos!'
    end
    if ns.detect { |n| n.class != Integer }
      raise ArgumentError, 'Apenas números!'
    end
    @numeros = ns
    @purchase = Time.now
  end

  def score(ticket)
    pontos = 0
    @numeros.each { |x| pontos += 1 if ticket.numeros.include?(x) }
    pontos
  end

  # def numeros; @numeros; end
  def numeros
    @numeros
  end

  def self.new_random
    new(rand(99), rand(99), rand(99))
  rescue ArgumentError
    retry
  end



end

# Lotérica
class LotteryDraw
  attr_accessor :tickets
  @@tickets = {}

  def LotteryDraw.buy_tickets(customer, *tickets)
    @@tickets[customer] = [] unless @@tickets.has_key?(customer)
    @@tickets[customer] += tickets
  end

# end
# class << LotteryDraw
  def LotteryDraw.play
    winlist = {}
    winner = Ticket::new_random
    p @@tickets
    @@tickets.each do |buyer, tickets|
      tickets.each do |t|
        score = t.score winner
        next if score.zero?
        winlist[buyer] ||= []
        winlist[buyer] << [t, score]
      end
    end
    winlist
  end
end

t = Ticket.new 10,55,94
p t
p t.numeros
# t.numeros = [1,2,3]
# p t.numeros
ticket_aleatorio = Ticket::new_random
LotteryDraw.buy_tickets 'Yal-dal-rip-sip',
                Ticket.new( 12, 6, 19 ),
                Ticket.new( 5, 1, 3 ),
                Ticket::new_random
ws = LotteryDraw.play
p ws



## Gambling with Fewer Fingers
#
class AnimalLottoTicket

  # A list of valid notes.
  NOTES = [:Ab, :A, :Bb, :B, :C, :Db, :D, :Eb, :E, :F, :Gb, :G]

  # Stores the three picked notes and a purchase date.
  attr_reader :picks, :purchased

  # Creates a new ticket from three chosen notes.  The three notes
  # must be unique notes.
  def initialize( note1, note2, note3 )
    picks = [note1, note2, note3]
    if [note1, note2, note3].uniq!
      raise ArgumentError, "the three picks must be different notes"
    elsif picks.detect { |p| not NOTES.include? p }
      raise ArgumentError, "the three picks must be notes in the chromatic scale."
    end
    @picks = picks
    @purchased = Time.now
    p NOTES
  end

  # Score this ticket against the final draw.
  def score( final )
    count = 0
    final.picks.each do |note|
      count +=1 if picks.include? note
    end
    count
  end

  # Constructor to create a random AnimalLottoTicket
  def self.new_random
    new( NOTES[ rand( NOTES.length ) ], NOTES[ rand( NOTES.length ) ],
         NOTES[ rand( NOTES.length ) ] )
  rescue ArgumentError
    retry
  end

end

p AnimalLottoTicket::NOTES
AnimalLottoTicket::new(:Ab, :A, :Bb)

#https://www.2n.pl/blog/basics-of-curses-library-in-ruby-make-awesome-terminal-apps
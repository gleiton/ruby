puts "Entre frase a criptografar:"
frase = gets.strip
final = ""

frase.each_char do |c|
  byte_cr =  c.ord + 1
  final << byte_cr.chr
end

puts "\nFrase final:\n#{final}"


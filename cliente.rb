class Cliente
  def initialize(nome, idade)
    @nome = nome
    @idade = idade
    debug_me
  end

  def debug_me
     puts "#{@nome},#{@idade}"
  end
end
=begin
class Cliente

  #declarando o metodo agora como privado
  private
  def debug_me
    puts "um outro metodo de debug"
  end
end
=end

Cliente.new("carlos", 30)
#Cliente.debug_me

c = Cliente.new("carlos", 55)
c.debug_me



module Contador
  attr_reader :maximo_necessario
  def << livro
    push livro if livro
    if @maximo_necessario == nil || size > @maximo_necessario
      @maximo_necessario = size
    end
    self
  end
end


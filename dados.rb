
def valores_dados
  puts 'Quantos dados?'
  # qt_dados = gets.to_i
  qt_dados = 2
  combinacoes = $dado.repeated_combination(qt_dados).map { |c| c }
  p combinacoes
  mapa_somas = {}
  for combinacao in combinacoes do
    soma = combinacao.sum
    if mapa_somas.key? soma
      mapa_somas[soma] += 1
    else
      mapa_somas[soma] = 1
    end
  end
  puts '┏━━━━┳━━━━━━━━━━━━━━┓'
  puts '┃SUM ┃ PROBABILIDADE┃'
  for linha in mapa_somas.keys
    puts '┃%3d ┃ ' % linha + "*"*mapa_somas[linha] + '┃'
  end
  puts '┗━━━━┻━━━━━━━━━━━━━━┛'
end

def anagramas palavra
  palavra.chars.permutation(palavra.size).map { |a| a.join }
end


def tira_parenteses frase
  # frase = frase.clone
  frase = frase.dup
  # unless frase.class == String
  unless frase.respond_to? :include?
    raise ArgumentError, "argumento #{frase.class} não dá!"
  end
  while frase.include?('(') && frase.include?(')')
    index_ini = frase.index '('
    index_fim = frase.index(')', index_ini)
    frase[index_ini..index_fim] = '' if index_fim
  end
  puts frase
end


$dado = [1, 2, 3, 4, 5, 6]


valores_dados


palavra = 'mango'
anagramas_m = anagramas palavra
puts "\n\nExistem #{anagramas_m.size} anagramas possíveis para a palavra #{palavra.upcase}:\n"
puts anagramas_m

frase_parenteses = 'Em lembrança aos (poucos) interessados e havendo (demasiados) esquecimentos.'
tira_parenteses frase_parenteses
puts frase_parenteses

# f = 2
# tira_parenteses(f)

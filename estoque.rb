require_relative 'livro'
require_relative 'contador'


=begin
class Array
    attr_reader :maximo_necessario
    def << (livro)
      push (livro)
      if @maximo_necessario.nil? || @maximo_necessario < size
        @maximo_necessario = size
      end
      self
    end
end
=end

class Estoque
  attr_reader :livros, :maximo_necessario

  def initialize
    @vendas = []
    @livros = []
    @livros.extend Contador
  end

=begin
  def @livros.maximo_necessario
    @maximo_necessario
  end
  def @livros.<< livro
    puts 'ADD'
    push livro if livro
    if @maximo_necessario == nil || size > @maximo_necessario
      @maximo_necessario = size
    end
    self
  end
=end

  def exporta_csv
    @livros.each do |livro|
      puts "#{livro.titulo},#{livro.ano}"
    end
  end
  
  def mais_barato_que(valor)
    @livros.select do |livro|
      livro.preco <= valor
    end
  end

  def adiciona(livro)
    @livros << livro if livro
  end
  def get_livros
  	@livros.clone
  end

  def remover obj
    @livros.delete obj
  end

  def livros_obter
  end

  def vende(livro)
    @livros.delete livro
    @vendas << livro
  end

  def quantidade_de_vendas_de_titulo(produto)
    @vendas.count { |venda| venda.titulo == produto.titulo }
  end
  def livro_que_mais_vendeu_por_titulo
    @vendas.sort {|v1,v2| quantidade_de_vendas_de_titulo(v1) <=> quantidade_de_vendas_de_titulo(v2)}.last
  end
  def livro_que_mais_vendeu_por_titulo2
    @vendas.sort {|v1,v2| quantidade_de_vendas_por(v1, &:titulo) <=> quantidade_de_vendas_por(v2, &:titulo)}.last
  end
  def livro_que_mais_vendeu_por_titulo3
    livro_que_mais_vendeu_por(&:titulo)
  end

  def method_missing(name)
    matcher = name.to_s.match "(.+)_que_mais_vendeu_por_(.+)"
    if matcher
      tipo = matcher[1]
        campo = matcher[2].to_sym  #pois precisamos converter para simbolo
        que_mais_vendeu_por(tipo, &campo)
    else
      super
    end 
  end
    def respond_to?(name)
    name.to_s.match ("(.+)_que_mais_vendeu_por_(.+)") || super
  end

  private
  def quantidade_de_vendas_por(produto, &campo)
    @vendas.count { |venda| campo.call(venda) == campo.call(produto) }
  end
  def livro_que_mais_vendeu_por(&campo)
    @vendas.sort {|v1,v2| quantidade_de_vendas_por(v1, &campo) <=> quantidade_de_vendas_por(v2, &campo)}.last
  end

end



module Decorator
  def imprime texto
    barra = "#" * 100
    puts barra
    puts texto
    puts barra
  end
end

module Bracos
  include Decorator
  def gancho_dir
    imprime "Vai um gancho de direita"
  end
  def jab_esq
    imprime "Vai um jab de esquerda"
  end
end

module Pernas
  include Decorator
  def rasteira
    imprime "Vai uma rasteira"
  end
  def chute_lateral
    imprime "Vai um bico"
  end
end

class Boxeador
  include Bracos
end

class Capoerista
  include Pernas
end

class Karateka
  include Bracos
  include Pernas
end

b = Boxeador::new
b.jab_esq

c = Capoerista::new
c.rasteira

k = Karateka::new
k.gancho_dir
k.chute_lateral


# MODULES AS NAMESPACES
module Reverse_World
  def self.diameter_reverse
    -44570
  end
  def self.puts texto
    print texto.reverse.to_s
  end
end

puts Reverse_World::diameter_reverse
puts "este texto foi escrito no mundo normal"
Reverse_World::puts "este texto foi escrito no mundo normal"

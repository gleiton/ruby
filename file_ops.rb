
def gravar(nome, texto)
  File::open(nome + '.txt', 'w') do |f|
    # r, w, a
    f << texto
  end
end

def ler(nome)
  File.read(nome)
  File::read nome
end

def renomear(nome_atual, nome_novo)
  File::rename nome_atual, nome_novo
end

def deletar(nome)
  File::delete nome
end

def ler_tudo
  Dir['*.*'].each do |nome|
    puts File::read nome
  end
end

def deliver(from: "A", to: nil, via: "mail")
  "Enviando de #{from} para #{to} através de #{via}."
end

deliver(to: "B")
# => "Enviando de A para B atavés de mail."
deliver(via: "Pony Express", from: "B", to: "A")
# => "Enviando de B para A através de Pony Express."
#
require_relative 'ui'


def escolher_palavra
	puts "\n\nEscolhendo palavra secreta aleatória"
	"programador".upcase!
end
def verifica_chute(chute, palavra, branco)
	$chutes << chute
	if chute.size > 1
		if chute == palavra
			puts "ACERTOU A PALAVRA"
			branco = palavra
		else
			puts "ERROU A PALAVRA"
		end
	else
		if palavra.include? chute
			puts "ACERTOU A LETRA!"
			for i in 1..palavra.size
				branco[i-1] = chute if palavra[i-1] == chute
			end
		else
			puts "ERROU A LETRA!"
		end
	end
	puts "\nATUAL: #{branco}\n"
	return branco
end

$max_tentativas = 5;
$chutes = [];
nome = da_boas_vindas
puts "Olá #{nome} vamos iniciar o jogo da forca com direito a #{$max_tentativas} tentativas.\n"
palavra = sorteia_palavra_secreta
branco = "-" * palavra.size
while $chutes.size < $max_tentativas
	puts "\n\n\n**** Tentativa número #{$chutes.size + 1} ****"
	puts "Entre uma letra ou logo a palavra final"
	branco = verifica_chute gets.strip.upcase!, palavra, branco
	if branco == palavra
		puts "OK você venceu!!"
		break
	end
end
puts "Rodadas terminadas. A palavra é: #{palavra}"



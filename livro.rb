class Float
  def para_dinheiro
    valor = "R$" << self.to_s.tr('.', ',')
    valor << "0" unless valor.match /(.*)(\d{2})$/
    valor
  end
end

class Livro
	attr_accessor :preco
	attr_reader :titulo, :ano
	def initialize(titulo, preco, ano, relancamento)
		@titulo = titulo
		@preco = preco
		@ano = ano
		@relancamento = relancamento
		#@preco *= 0.7 if ano < 1999
		verifica_desconto
	end
	def verifica_desconto
		@preco *= 0.7 if ano < 1999
	end
	def imprime_nf
		puts "Título: #{@titulo} - #{@ano}.......#{@preco}"
	end
	def calcula_preco(base)
		if @ano_lancamento < 2006
			if @possui_reimpressao
				base * 0.9
			else
				base * 0.95
			end
	    	elsif @ano_lancamento <= 2010
			base * 0.96
	    	else
			base
	    	end
	end
	def to_csv
		"#{@titulo},#{@ano_lancamento},#{@preco}"
	end
	def possui_reimpressao?
    @relancamento
  end
end

=begin
lr = Livro.new "Ruby sem mestre", 9.90, 1990, false
la = Livro.new "Algoritmo sem mestre", 15.00, 2001, true
lj = Livro.new "Java sem mestre", 14.00, 2010, true
livros = [lr, la, lj]
livros.each do |livro|
	livro.imprime_nf
end
=end

lr = Livro.new "Ruby sem mestre", 9.90, 1990, false
puts lr.possui_reimpressao?


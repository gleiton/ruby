def da_boas_vindas
    puts
    puts "        P  /_\\  P                              "
    puts "       /_\\_|_|_/_\\                             "
    puts "   n_n | ||. .|| | n_n         Bem vindo ao    "
    puts "   |_|_|nnnn nnnn|_|_|     Jogo de Adivinhação!"
    puts "  |' '  |  |_|  |'  ' |                        "
    puts "  |_____| ' _ ' |_____|                        " 
    puts "        \\__|_|__/                              "
    puts
    puts "Qual é o seu nome?"
    nome = gets.strip
    puts "\n\n\n\n\n\n"
    puts "Começaremos o jogo para você, #{nome}"
    nome
end

def pede_dificuldade
    puts "Qual o nível de dificuldade que deseja? (1 fácil, 5 difícil)"
    gets.to_i
end

def obtem_dificuldade
    case dificuldade
    when 1..2
        maximo = 30 * dificuldade
    when 3
        maximo = 100
    when 4
        maximo = 150
    else
        maximo = 200
    end
    return maximo
end

def sorteia_numero_secreto
    puts "Escolhendo um número secreto entre 0 e 200..."
    secreto = rand(200)
    puts "Escolhido... que tal adivinhar hoje nosso número secreto?"
    return secreto
end

def pede_um_numero(tentativa, limite_de_tentativas)
    puts "\n\n\n\n"
    puts "Tentativa " + tentativa.to_s + " de " + limite_de_tentativas.to_s
    puts "Entre com o número"
    chute = gets
    puts "Será que acertou? Você chutou #{chute}"
    chute.to_i
end

def verifica_se_acertou(numero_secreto, chute)
    acertou = numero_secreto == chute
    if acertou
        ganhou
        return true
    else
        maior = numero_secreto > chute
        if maior
            puts "O número secreto é maior!"
            puts $linha
            return false
        else
            puts "O número secreto é menor!"
            puts $linha
            return false
        end
    end
end

def ganhou
    puts
    puts "             OOOOOOOOOOO               "
    puts "         OOOOOOOOOOOOOOOOOOO           "
    puts "      OOOOOO  OOOOOOOOO  OOOOOO        "
    puts "    OOOOOO      OOOOO      OOOOOO      "
    puts "  OOOOOOOO  #   OOOOO  #   OOOOOOOO    "
    puts " OOOOOOOOOO    OOOOOOO    OOOOOOOOOO   "
    puts "OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO  "
    puts "OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO  "
    puts "OOOO  OOOOOOOOOOOOOOOOOOOOOOOOO  OOOO  "
    puts " OOOO  OOOOOOOOOOOOOOOOOOOOOOO  OOOO   "
    puts "  OOOO   OOOOOOOOOOOOOOOOOOOO  OOOO    "
    puts "    OOOOO   OOOOOOOOOOOOOOO   OOOO     "
    puts "      OOOOOO   OOOOOOOOO   OOOOOO      "
    puts "         OOOOOO         OOOOOO         "
    puts "             OOOOOOOOOOOO              "
    puts
    puts "               Acertou!                "
    puts
end


da_boas_vindas
numero_secreto = sorteia_numero_secreto
dificuldade = pede_dificuldade
puts "Dificuldade = #{dificuldade}"
chutes = []
limite_de_tentativas = 3
pontos_ate_agora = 1000
$linha = "==================================\n"
for tentativa in 1..limite_de_tentativas
	chute = pede_um_numero(tentativa, limite_de_tentativas)
	if verifica_se_acertou(numero_secreto, chute)
		break;
	end
	# unless !verifica_se_acertou(numero_secreto, chute)
	# break if verifica_se_acertou numero_secreto, chute
	#chutes << chute
	chutes[chutes.size] = chute
end
puts chutes
loop do
	break
end



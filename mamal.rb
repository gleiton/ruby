module Carnivoro
  attr_accessor :carnivoro
  
  def carnivoro?
    @carnivoro
  end
  
  def prey
    @prey
  end
end

class Mamal
  attr_accessor :peso, :altura, :som
  @@frescura = 1
  def initialize peso, altura
    @peso = peso
    @altura = altura
  end
  def self.grau_de_frescura
    @@frescura
  end
  def get_som
    @som * @@frescura
  end
end

pr = [:coelho, :ovelha, :galinha]

class Canidae < Mamal
  include Carnivoro

  def initialize peso, altura
    super peso, altura
    @carnivoro = true
  end
end

class Gato < Mamal
  @@frescura = 6
  def initialize peso, altura
    super peso, altura
    @som = "miau!"
  end
end

class Cao < Canidae
  @@frescura = 2
  def initialize peso, altura
    super peso, altura
    @som = "au!"
    @prey= :coelho
  end
  def count
    10
  end
end



c = Cao.new 20, 1
p c
puts c.carnivoro?
5.times { puts c.som }

3.times do
  puts " * #{c.som}"
end

for l in 'a'..'d' do
  puts " #{l} - #{c.som}"
end

alvo = if c.prey == :coelho
    "Coelho"
  else
    "Outro"
  end

puts alvo

puts Gato.grau_de_frescura
puts Cao.grau_de_frescura

puts Cao.new(20,2).get_som
puts Gato.new(10,1).get_som
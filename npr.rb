def escolher_op op, pilha
  case op
  when "+"
    pilha << (pilha.pop + pilha.pop)
  when "-"
    pilha << (-pilha.pop + pilha.pop)
  when "*"
    pilha << (pilha.pop * pilha.pop)
  when "/"
    divisor = pilha.pop
    dividendo = pilha.pop
    pilha << (dividendo / divisor)
  end
end


puts '='*100
puts '                  N O T A Ç Ã O   P O L O N E S A   R E V E R S A'
puts '='*100

pilha = []
ops = %w[+ - * /]
entrada = [2, 3, "+", 6, "*"]

for c in entrada do
  case c.class.to_s
  when "Integer"
    p pilha << c
  when "String"
    escolher_op c, pilha
    p pilha
  else
    puts 'nada'
  end
end

###################################
##########  COMPOSIÇÃO  ###########
###################################

module Produto
  def initialize(titulo, preco, ano_lancamento, editora)
     @titulo = titulo
     @preco = preco
     @ano_lancamento = ano_lancamento
     @editora = editora
  end
  # metodos
end

module Impresso
  def initialize(possui_reimpressao)
    @possui_reimpressao = possui_reimpressao
  end

  def possui_reimpressao?
    @possui_reimpressao
  end
end

class Livro
  #include Produto
  include Impresso
  include Produto
  
  def initialize(titulo, preco, ano_lancamento, possui_reimpressao, possui_sobrecapa, editora)
    super(titulo, preco, ano_lancamento, editora) # de quem é esse construtor?
    #super(possui_reimpressao) # de quem é esse construtor?
    @possui_sobrecapa = possui_sobrecapa
  end

  # metodos
end


###################################
############  HERANÇA  ############
###################################

class Produto2
  def initialize(titulo, preco, ano_lancamento, editora)
     # atribui variaveis
  end

  # metodos
end

module Impresso2
  def possui_reimpressao?
    @possui_reimpressao
  end
end

class Livro2 < Produto2
  include Impresso2

  def initialize(titulo, preco, ano_lancamento, possui_reimpressao, possui_sobrecapa, editora)
    super(titulo, preco, ano_lancamento, editora) # construtor de Produto
    @possui_reimpressao = possui_reimpressao
    @possui_sobrecapa = possui_sobrecapa
  end

  # metodos
end

class Revista < Produto2
  include Impresso2

  def initialize(titulo, preco, ano_lancamento, possui_reimpressao, numero, editora)
    super(titulo, preco, ano_lancamento, editora) # construtor de Produto
    @possui_reimpressao = possui_reimpressao
    @numero = numero
  end

  # metodos
end

class EBook < Produto2
  # o construtor vem de Produto

  # metodos
end


l = Livro.new("O programador pragmático", 100, 2000, true, true, "abril")
p l

l2 = Livro2.new("O programador pragmático", 100, 2000, true, true, "abril")
p l2



require 'set'

=begin
===============================================================
               METHODS
===============================================================
=end

def imprime p0
  p = p0.gsub "0", " "
  puts "\n ┏━━━┳━━━┳━━━┓ "
  puts " ┃ #{p[0]} ┃ #{p[1]} ┃ #{p[2]} ┃ "
  puts " ┣━━━╋━━━╋━━━┫ "
  puts " ┃ #{p[3]} ┃ #{p[4]} ┃ #{p[5]} ┃ "
  puts " ┣━━━╋━━━╋━━━┫ "
  puts " ┃ #{p[6]} ┃ #{p[7]} ┃ #{p[8]} ┃ "
  puts " ┗━━━┻━━━┻━━━┛\n"
end

def faz_movimentos p
  index_origem = p.index "0"
  $movimentos_possiveis_por_casa[index_origem].map do |index_final|
    nova_combinacao = p.clone
    nova_combinacao[index_origem] = nova_combinacao[index_final]
    nova_combinacao[index_final] = "0"
    nova_combinacao
  end
end

def is_valid p
  return false if p.size != 9 || p.size != p.chars.uniq.size
  for v in ("0".."8") do
    return false if !p.include? v
  end
  true
end

def is_predecessor p_ini, p_fim
  index_space_ini = p_ini.index "0"
  index_space_fim = p_fim.index "0"
  if $movimentos_possiveis_por_casa[index_space_ini].include? index_space_fim
    a0 = p_ini.chars
    af = p_fim.chars
    if a0.delete_at(index_space_fim) == af.delete_at(index_space_ini)
      a0.delete "0"
      af.delete "0"
      return a0 == af
    end
  end
  false
end

def trace_solution tree, trace
  tree.reverse_each do |nivel|
    for px in nivel do
      # if is_predecessor px, trace.last
      if is_predecessor px, trace.first
        # trace << px
        trace.unshift px
        break
      end
    end
  end
  # trace.reverse
  trace
end

$movimentos_possiveis_por_casa = {
  0 => [1, 3],
  1 => [0, 2, 4],
  2 => [1, 5],
  3 => [0, 4, 6],
  4 => [1, 3, 5, 7],
  5 => [2, 4, 8],
  6 => [3, 7],
  7 => [4, 6, 8],
  8 => [5, 7]
}


=begin
===============================================================
               M A I N  C O D E
===============================================================
=end

tempo_ini = Time::new
problema =  '605278134'
solucao = '012345678'
puts 'PROBLEMA:'
imprime problema
unless is_valid problema
  puts 'COMBINAÇÃO INVÁLIDA!!!'
  exit
end
tree = []
uniques = Set[problema]
puzzles = [problema]
resolvido = false

while !resolvido do
  tree << puzzles
  puts "NÍVEL-#{tree.size}:"
  nivel = []
  for puzzle in puzzles do
    novos = faz_movimentos(puzzle)
    if novos.include? solucao
      resolvido = true
      break
    else
      nivel += novos
    end
  end
  if resolvido
    puzzles = [solucao]
  else
    # puzzles = nivel.filter { |c| !uniques.include? c }
    puzzles = nivel.reject { |c| uniques.include? c }
    uniques += nivel.to_set
  end
end

tempo_fim = Time::new
puts "="*100
puts "        PROBLEMA CONCLUIDO EM #{(tempo_fim - tempo_ini).to_i} SEGs  !!!!!!!!!!!"
puts "        #{uniques.size} COMBINAÇÕES NÃO REPETIDAS ANALISADAS !!!"
puts "="*100

p trace_solution(tree, [solucao]).each { |s| imprime s }

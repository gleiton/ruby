

$tabula = (" ".."z").to_a
$b_ini = $tabula.first.ord
$b_fim = $tabula.last.ord
$keyword = "CHAVES"

$tab_morse = {
  "A" => ".-",
  "N" => "-.",
  "B" => "-...",
  "O" => "---",
  "C" => "-.-.",
  "P" => ".--.",
  "D" => "-..",
  "Q" => "--.-",
  "E" => ".",
  "R" => ".-.",
  "F" => "..-.",
  "S" => "...",
  "G" => "--.",
  "T" => "-",
  "H" => "....",
  "U" => "..-",
  "I" => "..",
  "V" => "...-",
  "J" => ".---",
  "W" => ".--",
  "K" => "-.-",
  "X" => "-..-",
  "L" => ".-..",
  "Y" => "-.--",
  "M" => "--",
  "Z" => "--.."
}


def cripto(frase)
  cifra = ""
  for p in 0..((frase.size) -1)
    char_frase = frase[p]
    char_chave = $keyword[((p +1) % $keyword.size) -1]
    char_cifra = cripto_char char_frase, char_chave, true
    cifra += char_cifra
    puts "#{p} #{frase[p]}  #{char_chave}  #{char_cifra}"
  end
  puts cifra
end
#RDS $31
#EC2 $55

def cripto2(frase, is_cripto = true)
  cifra = ""
  combinados = (1..(frase.size)).zip(frase.chars, ($keyword * (frase.size / $keyword.size + 1)).chars)
  combinados.each do |a|
    cifra << cripto_char(a[1], a[2], is_cripto)
  end
  p combinados
  p cifra
end

def cripto_char(char_frase, char_key, is_cripto)
  deslocamento = $tabula.index char_key
  pos_ini = $tabula.index char_frase
  pos_fim = if is_cripto
              pos_ini + deslocamento
            else
              pos_ini - deslocamento
            end
  pos_fim -= $tabula.size if pos_fim > $tabula.size - 1
  $tabula[pos_fim]
end

def cripto_morse(frase)
  frase.upcase!
  cifra = ""
  #for l in frase.chars
  frase.chars.each do |l|
    next if l == " "
    break if l == "*"
    cifra << if $tab_morse.include? l
               $tab_morse[l]
             else
               l
             end
  end
  puts cifra
end

def cripto_morse2(frase)
  $tab_morse.each do |key, value|
    frase.gsub! key, value
  end
  puts frase
end

puts "Entre frase original"
# cripto(gets.strip!)

cripto("Lupus homini lupus2")
cripto2("Lupus homini lupus2")
cripto2("oB6P=S0<3D8AC9;K?KU", false)

frase_raiz = "O RATO ROEU A ROUPA DO REI DE *ROMA"
cripto_morse frase_raiz
cripto_morse2 frase_raiz
puts frase_raiz

